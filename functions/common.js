const superagent = require('superagent');
var View = require("../views/View.js");
var Async = require("async");
var RSVP = require('rsvp');

function parseAddress(request, response) {

    // set header
    View.header(response);
    // set header title
    View.titleHeader(response);

    //Extraction Part
    const addresses = request.query.address;
    if (request.query.address instanceof Array) {
        for (let i = 0; i < addresses.length; i++) {
            var data;
            superagent.get(addresses[i]).then(res => {
                var xml = res.text;
                data = JSON.stringify(xml);
                var part = data.substring(
                    data.lastIndexOf("<title>") + 7,
                    data.lastIndexOf("</title")
                );
                const title = " " + addresses[i] + " - " + part + " ";
                View.title(response, title);
                if (addresses.length == (i + 1)) {
                    // set footer title
                    View.titleFooter(response);
                    // set footer
                    View.footer(response);
                }
            })
                .catch(err => {
                    const title = " " + addresses[i] + " -  NO RESPONSE ";
                    View.title(response, title);
                    if (addresses.length == (i + 1)) {
                        // set footer title
                        View.titleFooter(response);
                        // set footer
                        View.footer(response);
                    }
                })
        }
    } else {
        var data;
        superagent.get(addresses).then(res => {
            var xml = res.text
            data = JSON.stringify(xml);
            var part = data.substring(
                data.lastIndexOf("<title>") + 7,
                data.lastIndexOf("</title")
            );
            const title = " " + addresses + " - " + part + " ";
            View.title(response, title);
            // set footer title
            View.titleFooter(response);
            // set footer
            View.footer(response);
        })
            .catch(err => {
                const title = " " + addresses + " -  NO RESPONSE ";
                View.title(response, title);
                // set footer title
                View.titleFooter(response);
                // set footer
                View.footer(response);
            })
    }
    return View;
    // Extractions ends here
}

function parseAddressAsync(request, response) {
    var stack = [];
    // set header
    View.header(response);
    // set header title
    View.titleHeader(response);

    const addView = function (title, callback) {
        View.title(response, title);
        callback(null, true);
    };

    //Extraction Part
    const addresses = request.query.address;
    if (request.query.address instanceof Array) {
        for (let i = 0; i < addresses.length; i++) {
            var data;
            superagent.get(addresses[i]).then(res => {
                var xml = res.text
                data = JSON.stringify(xml);
                var part = data.substring(
                    data.lastIndexOf("<title>") + 7,
                    data.lastIndexOf("</title")
                );
                const title = " " + addresses[i] + " - " + part + " ";
                stack.push(title);
            })
                .catch(err => {
                    const title = " " + addresses[i] + " -  NO RESPONSE ";
                    stack.push(title);
                })
        }
    } else {
        var data;
        superagent
            .get(addresses)
            .then(res => {
                var xml = res.text
                data = JSON.stringify(xml);
                var part = data.substring(
                    data.lastIndexOf("<title>") + 7,
                    data.lastIndexOf("</title")
                );
                const title = " " + addresses + " - " + part + " ";
                stack.push(title);
            })
            .catch(err => {
                const title = " " + addresses + " -  NO RESPONSE ";
                stack.push(title);
            })
    }
    Async.all(stack, addView, function (err) {
        if (err) {
            console.log("error")
        }
        else {
            // set footer title
            View.titleFooter(response);
            // set footer
            View.footer(response);
        }
    });
    return View;
}

function parseAddressPromises(request, response) {
    let promises = [];
    // set header
    View.header(response);
    // set header title
    View.titleHeader(response);

    //Extraction Part
    const addresses = request.query.address;
    if (request.query.address instanceof Array) {

        for (let i = 0; i < addresses.length; i++) {


            const responseFrom = superagent
                .get(addresses[i])
                .then(res => {
                    const xml = res.text
                    const data = JSON.stringify(xml);
                    const part = data.substring(
                        data.lastIndexOf("<title>") + 7,
                        data.lastIndexOf("</title")
                    );
                    const title = " " + addresses[i] + " - " + part + " ";
                    return title;


                })
                .catch(err => {
                    const title = " " + addresses[i] + " -  NO RESPONSE ";
                    return title
                })
            // create promise for url
            promises.push(new RSVP.Promise(function (resolve, reject) {
                resolve(responseFrom);
            }));
        }
    } else {
        var data;
        const singleAdd = superagent
            .get(addresses)
            .then(res => {
                var xml = res.text
                data = JSON.stringify(xml);
                var part = data.substring(
                    data.lastIndexOf("<title>") + 7,
                    data.lastIndexOf("</title")
                );
                const title = " " + addresses + " - " + part + " ";
                return title;
            })
            .catch(err => {
                const title = " " + addresses + " -  NO RESPONSE ";
                return title;
            })

        // create promise for url
        promises.push(new RSVP.Promise(function (resolve, reject) {
            resolve(singleAdd);
        }));
    }
    RSVP.all(promises).then(function (responseText) {
        responseText.map(function (item) {
            View.title(response, item);
        });
        View.titleFooter(response);
        View.footer(response);
    });

    return View;
}
module.exports = { parseAddress, parseAddressAsync, parseAddressPromises }
